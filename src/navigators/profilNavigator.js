import { createStackNavigator, HeaderTitle } from 'react-navigation-stack';

import profileScreen from '../screens/profile'
import editProfilScreen from '../screens/editProfile'
import changePasswordScreen from '../screens/changePassword'
const profileNavigator = createStackNavigator({
    profileHome : {
        screen:profileScreen,
        navigationOptions:{
            headerShown:false
        }
    },
    editProfile:{
        screen:editProfilScreen,
        navigationOptions:{
            headerShown:false
        }
    },
    ChangePassword:{
        screen:changePasswordScreen,
        navigationOptions:{
            headerShown:false
        }
    }
})

export default profileNavigator