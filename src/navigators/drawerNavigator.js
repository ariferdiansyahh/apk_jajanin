import React, { Component } from 'react';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import {View} from 'react-native'
import homeScreen from '../screens/home'
import listBarangScreen from '../screens/listBarang'
import transactionScreen from '../screens/transaction'
import profileScreen from '../screens/profile'
import {Container, Header, Content, Icon} from 'native-base'
import transactionStack from './transactionNavigator'
import barangStack from './barangNavigator'
import profileStack from './profilNavigator'
import { COLORS } from '../services/config'
const bottomNavigator = createMaterialBottomTabNavigator({
    Home :{
        screen:homeScreen,
        navigationOptions:{
            tabBarLabel:'Home',
            tabBarIcon:() => (
                    <Icon name='home' style={{color:COLORS.WHITE_COLOR}}/>
            ),
            activeColor: COLORS.WHITE_COLOR,
            barStyle: { backgroundColor: COLORS.PRIMARY_COLOR} 
        }
    },
    transaction :{
        screen:transactionStack,
        navigationOptions:{
            tabBarLabel:'Transaksi',
            tabBarIcon:() => (
                <View>
                    <Icon name='cart' style={{color:COLORS.WHITE_COLOR}} />
                </View>
            ),
            activeColor: COLORS.WHITE_COLOR,
            barStyle: { backgroundColor: COLORS.PRIMARY_COLOR}
        }
    },
    listBarang :{
        screen:barangStack,
        navigationOptions:{
            tabBarLabel:'Barang',
            tabBarIcon:() => (
                <View>
                    <Icon name='list-box' style={{color:COLORS.WHITE_COLOR}}/>
                </View>
            ),
            activeColor: COLORS.WHITE_COLOR,
            barStyle: { backgroundColor: COLORS.PRIMARY_COLOR}
        }
    },
    Profile :{
        screen:profileStack,
        navigationOptions:{
            tabBarLabel:'Profile',
            tabBarIcon:() => (
                <View>
                    <Icon name='person' style={{color:COLORS.WHITE_COLOR}} />
                </View>
            ),
            activeColor: COLORS.WHITE_COLOR,
            barStyle: { backgroundColor: COLORS.PRIMARY_COLOR}
        }
    },
})

export default bottomNavigator