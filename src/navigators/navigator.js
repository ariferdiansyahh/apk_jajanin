import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator, HeaderTitle } from 'react-navigation-stack';
import LogoTittle from '../components/logo'
import loadingScreen from '../screens/loading'
import loginScreen from '../screens/login';
import registerScreen from'../screens/register'
import bottomNavigator from './drawerNavigator'
import {COLORS, FONTS} from '../services/config'

const AppNavigator = createStackNavigator({
    Loading:{
        screen:loadingScreen,
        navigationOptions:{
            headerShown:false
        }
    },
    Login: {
        screen: loginScreen,
        navigationOptions:{
            headerShown:false
        }
    },
    Register:{
        screen:registerScreen,
        navigationOptions:{
            headerStyle:{
                backgroundColor: COLORS.PRIMARY_COLOR,
            },
            headerTintColor:'#FFF'
        }
    },
    Home:{
        screen:bottomNavigator,
        navigationOptions:{
            headerStyle:{
                backgroundColor: COLORS.WHITE_COLOR,
            },
            headerTitle: () => <LogoTittle/>,
            headerTitleAlign:'center'
        }
    },
    
})

export default AppNavigator;