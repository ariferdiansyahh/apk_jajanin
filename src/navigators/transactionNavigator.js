import React, {Component} from 'react';  
import {createMaterialTopTabNavigator} from 'react-navigation-tabs'
import ViewPagerAdapter from 'react-native-tab-view-viewpager-adapter';
import {createAppContainer} from 'react-navigation';  
import transactionScreen from '../screens/transaction'
import listTransactionScreen from '../screens/listTransaksi'
import {COLORS} from '../services/config'
import scanQRScreen from '../components/scanQR'
const transactionNavigator = createMaterialTopTabNavigator(  
    {  
        Transaksi : transactionScreen,
        'List Transaksi' : listTransactionScreen,
        scanQR : {
            screen : scanQRScreen,
            navigationOptions:{
                headerShown:false
            }
        }
    },  
    {   tabBarOptions :{
        style:{
            backgroundColor:COLORS.PRIMARY_COLOR
        }
        },
        pagerComponent : ViewPagerAdapter
    }  
)  

export default transactionNavigator