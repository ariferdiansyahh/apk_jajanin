import { createStackNavigator, HeaderTitle } from 'react-navigation-stack';
import barangScreen from '../screens/listBarang'
import addBarangScreen from '../screens/addBarang'
import editBarangScreen from '../screens/editBarang'
import detailBarangScreen from '../screens/detailBarang'
const barangNavigator = createStackNavigator({
    listBarang : {
        screen:barangScreen,
        navigationOptions:{
            headerShown:false
        }
    },
    addBarang:{
        screen:addBarangScreen,
        navigationOptions:{
            headerShown:false
        }
    },
    editBarang:{
        screen:editBarangScreen,
        navigationOptions:{
            headerShown:false
        }
    },
    detailBarang :{
        screen:detailBarangScreen,
        
    }
})

export default barangNavigator