import { Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export const FULL_WIDTH = width;
export const COLORS = {
  WHITE_COLOR: '#FFF',
  BLACK_COLOR: '#000000',
  PRIMARY_COLOR: '#41A2C6',
  TRANSPARENT_COLOR: 'transparent',
  GRAY_COLOR: '#dddddd',
  RED_COLOR: '#CC3737',
  MEDIUM_BLACK: '#b63838',
  COLOR_INACTIVE: '#bdc3c7',
  BACKGROUND_COLOR: '#3F51B5',
  BLUE_HEADER: '#3F51B5',
  GREEN_COLOR : '#0c9463'
};

export const FONTS = {
  FAMILY: 'lato',
  SIZE: {
    SMALL: 12,
    SMALL_MEDIUM: 14,
    REGULAR: 16,
    MEDIUM: 20,
    LARGE: 24,
  },
};

export const BASE_URL = 'https://quiet-woodland-76876.herokuapp.com';
// export const VERSION_APP = 'V.1.0.7';
// export const COMPANY = 'IGOID BISNIS GLOBAL INC.';
// export const GROUP_COMPANY = 'IGOID BISNIS GLOBAL INC.';
