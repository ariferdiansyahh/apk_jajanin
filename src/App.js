/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import Navigator from './navigators/navigator';
import { createAppContainer } from 'react-navigation';
import { Provider } from 'react-redux';
import store from './store';

export default class App extends Component {
  render() {
    return (
        <Provider store={store}>
          <AppContainer/> 
        </Provider>
    );
  }
}

const AppContainer =  createAppContainer(Navigator);

