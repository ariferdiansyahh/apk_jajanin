import React, {Component} from 'react';
import { View, Text, Image, StyleSheet, Alert, ActivityIndicator, Keyboard } from 'react-native';
import {Form, Item, Input, Label, Button, Container, Content,Footer, FooterTab, Spinner} from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {FONTS, COLORS} from '../services/config'
import {registerAccount} from '../actions/register'
import { connect } from 'react-redux';

class Register extends Component{
    constructor(props){
      super(props)
      this.state = null
    }
    okBtn = () => {
      const {navigation} = this.props; 
    navigation.navigate('Login');
    }
    render(){
      onButtonRegister = async() => {
        Keyboard.dismiss()
        const data = await this.state
        if(data) {
          await this.props.registerAccount(data)
          if(this.props.register.error){
            Alert.alert('Message','Gagal')
          }else{
            
            Alert.alert('Message', 'Success', [
              {text:'OK', onPress : this.okBtn}
            ])
          }
        } else{
          Alert.alert('Message','Gagal')
        }
      }
        return(
        <Container style={{paddingHorizontal:20, backgroundColor:COLORS.WHITE_COLOR}}>
        <Content>
        <Text style={{fontSize: FONTS.SIZE.LARGE, fontWeight: 'bold', color: COLORS.PRIMARY_COLOR, marginLeft: 10,  marginTop:20}}>Create new account</Text>
        <Form>
            <Item floatingLabel>
              <Label>Email</Label>
              <Input onChangeText={(email) => this.setState({email})}/>
            </Item>
            <Item floatingLabel>
              <Label>Username</Label>
              <Input onChangeText={(username) => this.setState({username})}/>
            </Item>
            <Item floatingLabel>
              <Label>Password</Label>
              <Input secureTextEntry={true} onChangeText={(password) => this.setState({password})}/>
            </Item>
            <Item floatingLabel>
              <Label>Shop Name</Label>
              <Input onChangeText={(shop_name) => this.setState({shop_name})}/>
            </Item>
          </Form>
          <Button block success style={{marginTop:40,backgroundColor:COLORS.PRIMARY_COLOR, borderRadius: 5}}  onPress={() => onButtonRegister()}>
            <Text style={{color:COLORS.WHITE_COLOR, fontSize:18}}>Register</Text>
          </Button>    
        </Content>
                  {this.props.register.loading &&
                  <View style={styles.loading}>
                      <ActivityIndicator size="large" color={COLORS.BLUE_HEADER} />
                  </View>
                  }
      </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        flexDirection: 'column', 
        paddingHorizontal: 20, 
        paddingVertical: 20, 
        justifyContent: 'center', 
    },
    loading: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      opacity: 0.75,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center'
  },
    image: {
        height: 200, 
        width: 200, 
        alignSelf: 'center'
    },
    form: {
        paddingLeft: 20, 
        paddingRight: 20, 
        paddingBottom: 50,  
        marginTop: null, 
        borderRadius: 5
    },
    buttonList: {marginTop: 30, justifyContent: 'center', flexDirection: 'column'},
    signin: { borderRadius: 5},
    signinText: {fontWeight: 'bold', },
    or: {alignSelf: 'center', marginTop: 10},
    register: { fontWeight: 'bold', color:'black'}
})

const mapStateToProps = (state) => {
  return{
    register:state.registerState
  }
}
export default connect(mapStateToProps, {registerAccount})(Register)