import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import { Container, Header, View, Button, Icon, Fab, Content } from 'native-base';
import { NavigationActions, StackActions } from 'react-navigation';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import {COLORS, FONTS} from '../services/config'
import Card from '../components/detailCard'
const makanan = [
    {
        text: 'Mie Sedap',
        merchant: 'Mie ala Kos',
        price:"2500",
        type:"Makanan",
        image: require('../img/profil.png'),
    },
    {
        text: 'Mie Goreng',
        merchant: 'Mie ala Kos',
        price:"2000",
        type:"Makanan",
        image: require('../img/profil.png'),
    },
    {
        text: 'Mie Aceh',
        merchant: 'Burger Monster',
        price:"4500",
        type:"Makanan",
        image: require('../img/profil.png'),
    },
    {
        text: 'Mie Aceh',
        merchant: 'Burger Monster',
        price:"4500",
        type:"Makanan",
        image: require('../img/profil.png'),
    },
    {
        text: 'Mie Aceh',
        merchant: 'Burger Monster',
        price:"4500",
        type:"Makanan",
        image: require('../img/profil.png'),
    },
    {
        text: 'Mie Aceh',
        merchant: 'Burger Monster',
        price:"4500",
        type:"Makanan",
        image: require('../img/profil.png'),
    },
    {
        text: 'Mie Aceh',
        merchant: 'Burger Monster',
        price:"4500",
        type:"Makanan",
        image: require('../img/profil.png'),
    },
    {
        text: 'Mie Aceh',
        merchant: 'Burger Monster',
        price:"4500",
        type:"Makanan",
        image: require('../img/profil.png'),
    },
    {
        text: 'Mie Aceh',
        merchant: 'Burger Monster',
        price:"4500",
        type:"Makanan",
        image: require('../img/profil.png'),
    }
    
    ];
class listBarang extends Component {
    
    render(){
        
        return(
            <Container>
                <View style={{marginHorizontal:25, marginTop:20}}>
                    <Text style={{fontSize:20}}>Transaksi Terakhir</Text>
                    <FlatList style={{marginBottom:35}}  
                        showsVerticalScrollIndicator={false}                      
                        data={makanan} keyExtractor={item => item.text}
                        renderItem={({item}) => (
                            <Card name={item.text} price={item.price} imageUri={item.image} merchant type={item.type} ></Card>
                        )}
                    />
                    
                </View>

            </Container>
        )
    }
    
}

const styles = StyleSheet.create({  
    container: {  
        flex: 1,  
        justifyContent: 'center',  
        alignItems: 'center'  
    },  
});  

export default listBarang