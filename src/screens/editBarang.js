import React, {Component} from 'react';
import { Container, Content,Button,Form,Item,Input,Label, Text, Picker, Icon, View} from 'native-base';
import {COLORS, FONTS} from '../services/config'
class editBarang extends Component {
constructor(props) {
    super(props);
    this.state = {
        selected: "key1"
    };
    }
    onValueChange(value) {
    this.setState({
        selected: value
    });
    }
    render(){
        return(
            <Container style={{paddingHorizontal:20}}>
                <Content>
                <Text style={{fontSize: FONTS.SIZE.LARGE, fontWeight: 'bold', color: COLORS.PRIMARY_COLOR, marginLeft: 10,  marginTop:20}}>Edit Barang</Text>
                    <Form>
                        <Item floatingLabel style={{marginVertical:10}}>
                            <Label>Nama Barang</Label>
                            <Input value="Mie Sedap"></Input>
                        </Item>
                        <Item style={{marginVertical:10}}>
                        <Label>Jenis Barang : </Label>
                        <Picker
                        mode="dropdown"
                        iosHeader="Select your SIM"
                        iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}
                        style={{ width: undefined }}
                        selectedValue={this.state.selected}
                        onValueChange={this.onValueChange.bind(this)}
                        >
                        <Picker.Item label="Makanan" value="key0" />
                        <Picker.Item label="Minuman" value="key1" />
                        </Picker>
                        </Item>
                        <Item floatingLabel style={{marginVertical:5}}>
                            <Label>Harga</Label>
                            <Input value="5000"></Input>
                        </Item>
                        <Item floatingLabel style={{marginVertical:5}}>
                            <Label>Total Barang</Label>
                            <Input value="20"></Input>
                        </Item>
                    </Form>
                    <View style={{marginVertical:20}}>

                    <Button block iconRight style={{backgroundColor:COLORS.PRIMARY_COLOR, borderRadius:10}}>
                        <Text>Edit Barang</Text>
                        <Icon name="arrow-forward"></Icon>
                    </Button>
                    </View>
                </Content>
            </Container>
        )
    }
}

export default editBarang