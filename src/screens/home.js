import React, {Component} from 'react';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import { Container, Content, Card, CardItem, Text, Body, Picker, Icon } from 'native-base'
import { NavigationActions, StackActions } from 'react-navigation';
import {COLORS, BASE_URL} from '../services/config'
import axios from 'axios'
class home extends Component {
    render(){
        return(
            <Container >
                <Content padder style={{marginHorizontal:15}} showsVerticalScrollIndicator={false}>
                    <Content style={styles.card}>
                    <Card style={{borderRadius:10}}>
                        <CardItem header bordered  style={{borderRadius:10}}>
                            <Icon name="cash" style={{color:COLORS.PRIMARY_COLOR}}></Icon>
                        <Text style={{fontSize:20, color:COLORS.PRIMARY_COLOR}}>PENDAPATAN</Text>
                        </CardItem>
                        <CardItem bordered   style={{backgroundColor:COLORS.PRIMARY_COLOR, borderBottomLeftRadius:10,  borderBottomRightRadius:10}}>
                        <Body>
                        <Text style={{alignSelf:'center', fontSize:25, marginVertical:30, color:'#fff'}}>Rp. 2000000</Text>
                        </Body>
                        </CardItem>
                        
                    </Card>

                    <View style={{marginTop:20}}>
                    <Card style={{borderRadius:10}}>
                        <CardItem header bordered  style={{borderRadius:10}}>
                            <Icon name="refresh" style={{color:COLORS.RED_COLOR}}></Icon>
                        <Text style={{fontSize:20, color:COLORS.RED_COLOR}}>BARANG HABIS</Text>
                        </CardItem>
                        <CardItem bordered   style={{backgroundColor:COLORS.RED_COLOR, borderBottomLeftRadius:10,  borderBottomRightRadius:10}}>
                        <Body>
                        <Text style={{alignSelf:'center', fontSize:25, marginVertical:30, color:'#fff'}}>2</Text>
                        </Body>
                        </CardItem>
                        
                    </Card>
                    <View style={{marginTop:20}}>
                    <Card style={{borderRadius:10}}>
                        <CardItem header bordered  style={{borderRadius:10}}>
                            <Icon name="cart" style={{color:COLORS.GREEN_COLOR}}></Icon>
                        <Text style={{fontSize:20, color:COLORS.GREEN_COLOR}}>TOTAL TRANSAKSI</Text>
                        </CardItem>
                        <CardItem bordered   style={{backgroundColor:COLORS.GREEN_COLOR, borderBottomLeftRadius:10,  borderBottomRightRadius:10}}>
                        <Body>
                        <Text style={{alignSelf:'center', fontSize:25, marginVertical:30, color:'#fff'}}>20</Text>
                        </Body>
                        </CardItem>
                        
                    </Card>
                    </View>

                    <View style={{marginTop:20}}>

                    <Card style={{borderRadius:10}}>
                        <CardItem header bordered  style={{borderRadius:10}}>
                            <Icon name="clipboard" style={{color:'#504658'}}></Icon>
                        <Text style={{fontSize:20, color:'#504658'}}>TOTAL ITEM</Text>
                        </CardItem>
                        <CardItem bordered   style={{backgroundColor:'#504658', borderBottomLeftRadius:10,  borderBottomRightRadius:10}}>
                        <Body>
                        <Text style={{alignSelf:'center', fontSize:25, marginVertical:30, color:'#fff'}}>11</Text>
                        </Body>
                        </CardItem>
                        
                    </Card>
                    </View>
                    </View>
                    </Content>
                    
                </Content>
            </Container>
        )
    }
    
}

const styles = StyleSheet.create({  
    container: {  
        flex: 1,  
        justifyContent: 'center',  
        alignItems: 'center'  
    },
    
    card:{
        marginVertical:15,
        
    }
});  

export default home