import React, {Component} from 'react';
import {Image, StyleSheet, TouchableOpacity, ImageBackground} from 'react-native';
import {connect} from 'react-redux';
import { Text, Body, View, Thumbnail, Form,Button,Item, Label, Input, List, ListItem, Left, Right, Icon, Container, Content  } from 'native-base'
import {COLORS, FONTS} from '../services/config'
import QRCode from 'react-native-qrcode-svg';
import { NavigationActions, StackActions } from 'react-navigation';
class detailBarang extends Component {
    async _logout(){
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Login' })],
        });
        this.props.navigation.dispatch(resetAction)
    }
    render(){
        return(
            <Container>
                <Content>
                <View style={{backgroundColor:COLORS.WHITE_COLOR}}>
                <View style={{marginVertical:20}}>
                    <List>
                        <ListItem avatar>
                        <Left>
                            <Icon style={{fontSize:20}} active name="restaurant"/>
                        </Left>
                        <Body>
                            <Text>NAMA BARANG</Text>
                            <Text note>Mie Sedap</Text>
                        </Body>
                        </ListItem>
                    <ListItem avatar>
                        <Left>
                            <Icon style={{fontSize:20}} active name="cafe"/>
                        </Left>
                        <Body>
                            <Text>JENIS</Text>
                            <Text note>Makanan</Text>
                        </Body>
                    </ListItem>
                    <ListItem avatar>
                        <Left>
                            <Icon style={{fontSize:20}} active name="logo-usd"/>
                        </Left>
                        <Body>
                            <Text>HARGA</Text>
                            <Text note>Rp. 5000</Text>
                        </Body>
                    </ListItem>
                    <ListItem avatar>
                        <Left>
                            <Icon style={{fontSize:20}} active name="clipboard"/>
                        </Left>
                        <Body>
                            <Text>TOTAL BARANG</Text>
                            <Text note>20</Text>
                        </Body>
                    </ListItem>
                    <ListItem avatar>
                        <Left>
                            <Icon style={{fontSize:20}} active name="ios-qr-scanner"/>
                        </Left>
                        <Body>
                            <Text>QR CODE</Text>
                            <View style={{marginVertical:10}}>
                            <QRCode
                            size={150}
                            value={JSON.stringify({test: 'testdata'})}
                            />
                            </View>
                            <Button block style={{width:200}}><Text>Unduh QR</Text></Button>
                        </Body>
                    </ListItem>
                    </List>
                    <View style={{marginHorizontal:30}}>
                    <TouchableOpacity>
                    <Button block success style={{marginTop:20,backgroundColor:COLORS.PRIMARY_COLOR, borderRadius: 5}}  onPress={() => this.props.navigation.navigate('editBarang')}>
                        <Text style={{color:COLORS.WHITE_COLOR}} >EDIT MENU</Text>
                    </Button>    
                    </TouchableOpacity>
                    <TouchableOpacity>   
                    </TouchableOpacity>
                    <TouchableOpacity>
                    <Button block success style={{marginTop:20,backgroundColor:COLORS.RED_COLOR, borderRadius: 5}} >
                        <Text style={{color:COLORS.WHITE_COLOR}}>DELETE MENU</Text>
                    </Button>    
                    </TouchableOpacity>          
                    </View>
                </View>
            </View>
                </Content>
            </Container>
        )
    }
    
}

const styles = StyleSheet.create({  
    container: {flex: 1, flexDirection: 'column', paddingHorizontal: 20, paddingVertical: 20, justifyContent: 'center', backgroundColor: COLORS.WHITE_COLOR},
    image: {height: 150, width: 150, borderRadius:150/2},
    form: {paddingLeft: 20, paddingRight: 20, paddingBottom: 50, backgroundColor: COLORS.WHITE_COLOR, marginTop: null, borderRadius: 5 , },
    buttonList: {marginTop: 30, justifyContent: 'center', flexDirection: 'column'},
    signin: {backgroundColor: COLORS.WHITE_COLOR, borderRadius: 5},
    signinText: {fontWeight: 'bold', fontSize: FONTS.SIZE.MEDIUM, color: COLORS.PRIMARY_COLOR},
    or: {alignSelf: 'center', color: COLORS.GRAY_COLOR, fontSize: FONTS.SIZE.REGULAR, marginTop: 10},
    register: {color: COLORS.WHITE_COLOR, fontSize: FONTS.SIZE.REGULAR, fontWeight: 'bold', textDecorationLine:'underline'}
});  

export default detailBarang