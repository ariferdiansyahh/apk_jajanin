import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import { Container, Header, View, Button, Icon, Fab, Content } from 'native-base';
import { NavigationActions, StackActions } from 'react-navigation';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import {COLORS, FONTS} from '../services/config'
import Card from '../components/cardItem'
const makanan = [
    {
        text: 'Mie Sedap',
        merchant: 'Mie ala Kos',
        price:"2500",
        type:"Makanan",
        image: require('../img/profil.png'),
    },
    {
        text: 'Mie Goreng',
        merchant: 'Mie ala Kos',
        price:"2000",
        type:"Makanan",
        image: require('../img/profil.png'),
    },
    {
        text: 'Mie Aceh',
        merchant: 'Burger Monster',
        price:"4500",
        type:"Makanan",
        image: require('../img/profil.png'),
    },
    ];
    const minuman = [
        {
            text: 'Teh Gelas',
            merchant: 'Mie ala Kos',
            price:"2500",
            type:"Makanan",
            image: require('../img/profil.png'),
        },
        {
            text: 'Yakult',
            merchant: 'Mie ala Kos',
            price:"2000",
            type:"Makanan",
            image: require('../img/profil.png'),
        },
        {
            text: 'Es Mambo',
            merchant: 'Burger Monster',
            price:"4500",
            type:"Makanan",
            image: require('../img/profil.png'),
        },
        ];  
class listBarang extends Component {
    render(){
        return(
            <Container>
                <Content>
                <View style={{marginHorizontal:25, marginTop:20}}>
                    <Text style={{fontSize:20, fontWeight:'bold'}}>Makanan</Text>
                    <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={makanan} keyExtractor={item => item.text}
                        renderItem={({item}) => (
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('detailBarang')}>
                                <Card name={item.text} price={item.price} imageUri={item.image} merchant type={item.type} ></Card>
                            </TouchableOpacity>
                        )}
                    />
                    
                </View>
                <View style={{marginHorizontal:25, marginTop:20}}>
                    <Text style={{fontSize:20, fontWeight:'bold'}}>Minuman</Text>
                    <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={minuman} keyExtractor={item => item.text}
                        renderItem={({item}) => (
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('detailBarang')}>
                                <Card name={item.text} price={item.price} imageUri={item.image} merchant type={item.type} ></Card>
                            </TouchableOpacity>
                        )}
                    />
                    
                </View>
                <View style={{marginHorizontal:25, marginTop:20 , marginBottom:50}}>
                    <Text style={{fontSize:20, fontWeight:'bold'}}>Barang lainnya</Text>
                    <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={minuman} keyExtractor={item => item.text}
                        renderItem={({item}) => (
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('detailBarang')}>
                                <Card name={item.text} price={item.price} imageUri={item.image} merchant type={item.type} ></Card>
                            </TouchableOpacity>
                        )}
                    />
                    
                </View>
                </Content>
                <Fab
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: COLORS.PRIMARY_COLOR }}
                    position="bottomRight"
                    onPress={() => this.props.navigation.navigate('addBarang')}>
                    <Icon name="add" />
                </Fab>
            </Container>
        )
    }
    
}

const styles = StyleSheet.create({  
    container: {  
        flex: 1,  
        justifyContent: 'center',  
        alignItems: 'center'  
    },  
});  

export default listBarang