import React, {Component} from 'react';
import {View, Image, StyleSheet, Text} from 'react-native';
import {connect} from 'react-redux';
import  {Container, Content, Icon,Button , Input} from 'native-base'
import { NavigationActions, StackActions } from 'react-navigation';
import {COLORS, FONTS} from '../services/config'
import { CalculatorInput } from 'react-native-calculator'
import ScanQr from '../components/scanQR'
class transaction extends Component {
    constructor(props){
        super(props)
        this.state = {
            price: 56000
        }
    }

    render(){
        return(
            <Container>
                <Content  style={{marginHorizontal:30}}>
                <Text style={styles.title}>Total Orders</Text>
                    <View style={{flexDirection:'column',borderBottomWidth: 1,borderBottomColor: COLORS.GRAY_COLOR, marginVertical:10}}>
                        <View style={styles.items}>
                            <View style={{flexDirection:'row', marginTop:-10}}>
                            <Button rounded style={{height:40, width:40, backgroundColor:COLORS.PRIMARY_COLOR}}>
                                <Icon name='remove' style={{fontSize:15}}></Icon>
                            </Button>
                            <Text style={{marginTop:10, marginHorizontal:5, fontWeight:'bold'}}>10</Text>
                            <Button rounded style={{height:40, width:40, backgroundColor:COLORS.PRIMARY_COLOR}}>
                                <Icon name='add' style={{fontSize:15}}></Icon>
                            </Button>
                            <Text style={{fontSize:18, marginHorizontal:5, marginTop:8,fontSize: FONTS.SIZE.REGULAR,color: COLORS.BLACK_COLOR,fontWeight: 'bold'}}>Mie Sedap</Text>
                            </View>
                                <Text style={styles.text}>Rp 5000</Text>
                        </View>
                        <Text style={{fontSize: FONTS.SIZE.REGULAR,color: COLORS.BLACK_COLOR,fontWeight: 'bold'}}>Total : Rp 50000</Text>
                    </View>
                    <View style={{flexDirection:'column',borderBottomWidth: 1,borderBottomColor: COLORS.GRAY_COLOR, marginVertical:10}}>
                        <View style={styles.items}>
                            <View style={{flexDirection:'row', marginTop:-10}}>
                            <Button rounded style={{height:40, width:40, backgroundColor:COLORS.PRIMARY_COLOR}}>
                                <Icon name='remove' style={{fontSize:15}}></Icon>
                            </Button>
                            <Text style={{marginTop:10, marginHorizontal:5, fontWeight:'bold'}}>2</Text>
                            <Button rounded style={{height:40, width:40, backgroundColor:COLORS.PRIMARY_COLOR}}>
                                <Icon name='add' style={{fontSize:15}}></Icon>
                            </Button>
                            <Text style={{fontSize:18, marginHorizontal:5, marginTop:8,fontSize: FONTS.SIZE.REGULAR,color: COLORS.BLACK_COLOR,fontWeight: 'bold'}}>Teh Gelas</Text>
                            </View>
                                <Text style={styles.text}>Rp 3000</Text>
                        </View>
                        <Text style={{fontSize: FONTS.SIZE.REGULAR,color: COLORS.BLACK_COLOR,fontWeight: 'bold'}}>Total : Rp 6000</Text>
                    </View>
                    
                    
                <View style={styles.itemsPrice}>
                <Text>Total Harga : </Text>
                <View style={styles.itemsPrice2}>
                <Text style={{fontSize:20, marginTop:15}}>Rp</Text>
                <CalculatorInput
                fieldTextStyle={{ fontSize: 24 }}
                value={this.state.price}
                modalAnimationType={"slide"}
                onChange={(price) => this.setState({price})}
                fieldContainerStyle={{ height: 36 }}
                />
                </View>
                </View>
                <Button block style={{backgroundColor:COLORS.PRIMARY_COLOR, borderRadius:10}}onPress={() => this.props.navigation.navigate('scanQR')}>
                    <Text style={{fontWeight:'bold', color:'#fff'}}>SCAN QR</Text>
                </Button>
                </Content>
            </Container>

        )
    }
    
}

const styles = StyleSheet.create({  
    container: {  
        flex: 1,  
        backgroundColor:COLORS.WHITE_COLOR  
    },
    text: {
        fontSize: FONTS.SIZE.REGULAR,
        color: COLORS.BLACK_COLOR,
        fontWeight: 'bold'
    },
    items: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 15,        

    },
    itemsPrice: {
        marginVertical:20,
        flexDirection: 'column',      
        borderBottomColor: COLORS.GRAY_COLOR,
        borderBottomWidth: 1,
    },
    itemsPrice2: {
        flexDirection: 'row',     
    },
    title:{
        fontSize: FONTS.SIZE.LARGE,
        color: COLORS.BLACK_COLOR,
        fontWeight: 'bold',
        marginVertical: 20
    },  
});  

export default transaction