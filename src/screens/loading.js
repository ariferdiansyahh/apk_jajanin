import React, {Component} from 'react';
import {View, Image, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import { NavigationActions, StackActions } from 'react-navigation';
import {COLORS} from '../services/config'
import {getSession} from '../actions/session'
class loading extends Component{
    componentDidMount(){
        this.checkSession();
    }
    async checkSession(){
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Login' })],
        });
        await this.props.getSession()
        setTimeout(()=> {
            (this.props.session.token === null ? this.props.navigation.dispatch(resetAction) : this._init())
            ; 
        }, 0)
    }
    async _init(){
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Home' })],
            
        });
        this.props.navigation.dispatch(resetAction)
    }
    render(){
        return(
            <View style={styles.container}>
                <Image source={require('../img/logo-white.png')} style={styles.image} resizeMode='contain'/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:COLORS.PRIMARY_COLOR,
        flex:1,
        justifyContent: 'center'
    },
    image:{
        alignSelf:'center',
        height:200,
        width:200
    }
})

const mapStateToProps = (state) => {
    return{
    session:state.sessionState
    }
}

export default connect(mapStateToProps,{getSession})(loading);