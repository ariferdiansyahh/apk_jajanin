import React, {Component} from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import {Form, Item, Input, Label, Button} from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { COLORS, FONTS } from '../services/config'
import { NavigationActions, StackActions } from 'react-navigation';
class Login extends Component{
    async _login(){
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Home' })],
        });
        this.props.navigation.dispatch(resetAction)
    }
    render(){
        return(
            <View style={styles.container}>
                <Image source={require('../img/logo-white.png')} style={styles.image} resizeMode='contain'/>
                    <View style={styles.form}>
                        <Form>
                            <Item floatingLabel>
                                <Label>E-mail</Label>
                                <Input></Input>
                            </Item>
                            <Item floatingLabel>
                                <Label>Password</Label>
                                <Input></Input>
                            </Item>
                        </Form>
                    </View>
                                    <View style={styles.buttonList}>
                    <TouchableOpacity onPress={() => this._login() }>
                        <Button block style={styles.signin}>
                            <Text style={styles.signinText}>Login</Text>
                        </Button>
                    </TouchableOpacity>                    
                    <Text style={styles.or}>Tidak punya akun ?</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                        <Button transparent block style={{margin:-10}}>
                            <Text style={styles.register}>Register</Text>
                        </Button>
                    </TouchableOpacity>                    
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {flex: 1, flexDirection: 'column', paddingHorizontal: 20, paddingVertical: 20, justifyContent: 'center', backgroundColor: COLORS.PRIMARY_COLOR},
    image: {height: 200, width: 200, alignSelf: 'center'},
    form: {paddingLeft: 20, paddingRight: 20, paddingBottom: 50, backgroundColor: COLORS.WHITE_COLOR, marginTop: null, borderRadius: 5 , },
    buttonList: {marginTop: 30, justifyContent: 'center', flexDirection: 'column'},
    signin: {backgroundColor: COLORS.WHITE_COLOR, borderRadius: 5},
    signinText: {fontWeight: 'bold', fontSize: FONTS.SIZE.MEDIUM, color: COLORS.PRIMARY_COLOR},
    or: {alignSelf: 'center', color: COLORS.GRAY_COLOR, fontSize: FONTS.SIZE.REGULAR, marginTop: 10},
    register: {color: COLORS.WHITE_COLOR, fontSize: FONTS.SIZE.REGULAR, fontWeight: 'bold', textDecorationLine:'underline'}
})

export default Login