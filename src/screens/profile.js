import React, {Component} from 'react';
import {Image, StyleSheet, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import { Text, Body, View, Thumbnail, Form,Button,Item, Label, Input, List, ListItem, Left, Right, Icon  } from 'native-base'
import {COLORS, FONTS} from '../services/config'
import { NavigationActions, StackActions } from 'react-navigation';
class profile extends Component {
    async _logout(){
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Login' })],
        });
        this.props.navigation.dispatch(resetAction)
    }
    render(){
        return(
            <View style={{backgroundColor:COLORS.WHITE_COLOR}}>
                <View style={{backgroundColor:COLORS.PRIMARY_COLOR, alignItems:'center'}} >
                    <View style={{marginVertical:25}}>
                    <Image source={require('../img/profil.png')} style={styles.image} />
                    <Text style={{alignSelf: 'center', color:COLORS.WHITE_COLOR, marginTop:10, fontSize:FONTS.SIZE.REGULAR}}>ARI FERDIANSYAH</Text>
                    </View>
                </View>
                <View style={{marginVertical:20}}>
                    <List>
                        <ListItem avatar>
                        <Left>
                            <Icon style={{fontSize:20}} active name="mail"/>
                        </Left>
                        <Body>
                            <Text>EMAIL</Text>
                            <Text note>ariferdiansyah110@gmail.com</Text>
                        </Body>
                        </ListItem>
                    <ListItem avatar>
                        <Left>
                            <Icon style={{fontSize:20}} active name="call"/>
                        </Left>
                        <Body>
                            <Text>PHONE</Text>
                            <Text note>085777798433</Text>
                        </Body>
                    </ListItem>
                    <ListItem avatar>
                        <Left>
                            <Icon style={{fontSize:20}} active name="person"/>
                        </Left>
                        <Body>
                            <Text>USERNAME</Text>
                            <Text note>ariferdiansyahh</Text>
                        </Body>
                    </ListItem>
                    </List>
                    <View style={{marginHorizontal:30}}>
                    <TouchableOpacity>
                    <Button block success style={{marginTop:20,backgroundColor:COLORS.PRIMARY_COLOR, borderRadius: 5}}  onPress={() => this.props.navigation.navigate('editProfile')}>
                        <Text style={{color:COLORS.WHITE_COLOR}} >EDIT PROFILE</Text>
                    </Button>    
                    </TouchableOpacity>
                    <TouchableOpacity>
                    <Button block success style={{marginTop:20,backgroundColor:COLORS.GRAY_COLOR, borderRadius: 5}} onPress={() => this.props.navigation.navigate('ChangePassword')}>
                        <Text style={{color:COLORS.BLACK_COLOR}} >CHANGE PASSWORD</Text>
                    </Button>    
                    </TouchableOpacity>
                    <TouchableOpacity>
                    <Button block success style={{marginTop:20,backgroundColor:COLORS.RED_COLOR, borderRadius: 5}} onPress={() => this._logout()} >
                        <Text style={{color:COLORS.WHITE_COLOR}}>LOGOUT</Text>
                    </Button>    
                    </TouchableOpacity>          
                    </View>
                   
                </View>
            </View>
        )
    }
    
}

const styles = StyleSheet.create({  
    container: {flex: 1, flexDirection: 'column', paddingHorizontal: 20, paddingVertical: 20, justifyContent: 'center', backgroundColor: COLORS.WHITE_COLOR},
    image: {height: 150, width: 150, borderRadius:150/2},
    form: {paddingLeft: 20, paddingRight: 20, paddingBottom: 50, backgroundColor: COLORS.WHITE_COLOR, marginTop: null, borderRadius: 5 , },
    buttonList: {marginTop: 30, justifyContent: 'center', flexDirection: 'column'},
    signin: {backgroundColor: COLORS.WHITE_COLOR, borderRadius: 5},
    signinText: {fontWeight: 'bold', fontSize: FONTS.SIZE.MEDIUM, color: COLORS.PRIMARY_COLOR},
    or: {alignSelf: 'center', color: COLORS.GRAY_COLOR, fontSize: FONTS.SIZE.REGULAR, marginTop: 10},
    register: {color: COLORS.WHITE_COLOR, fontSize: FONTS.SIZE.REGULAR, fontWeight: 'bold', textDecorationLine:'underline'}
});  

export default profile