import React, {Component} from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import {Form, Item, Input, Label, Button, Container, Content,Footer, FooterTab} from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {FONTS, COLORS} from '../services/config'

class changePassword extends Component{
    render(){
        return(
        <Container style={{paddingHorizontal:20}}>
        <Content>
        <Text style={{fontSize: FONTS.SIZE.LARGE, fontWeight: 'bold', color: COLORS.PRIMARY_COLOR, marginLeft: 10,  marginTop:20}}>Ganti Password</Text>
        <Form>
            <Item floatingLabel>
              <Label>New Password</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label>Confirm Password</Label>
              <Input/>
            </Item>
          </Form>
          
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Button block success style={{marginTop:40,backgroundColor:COLORS.PRIMARY_COLOR, borderRadius: 5}}>
            <Text style={{color:COLORS.WHITE_COLOR, fontSize:16}}>GANTI PASSWORD</Text>
          </Button>    
          </TouchableOpacity>        
        </Content>
      </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        flexDirection: 'column', 
        paddingHorizontal: 20, 
        paddingVertical: 20, 
        justifyContent: 'center', 
    },
    image: {
        height: 200, 
        width: 200, 
        alignSelf: 'center'
    },
    form: {
        paddingLeft: 20, 
        paddingRight: 20, 
        paddingBottom: 50,  
        marginTop: null, 
        borderRadius: 5
    },
    buttonList: {marginTop: 30, justifyContent: 'center', flexDirection: 'column'},
    signin: { borderRadius: 5},
    signinText: {fontWeight: 'bold', },
    or: {alignSelf: 'center', marginTop: 10},
    register: { fontWeight: 'bold', color:'black'}
})

export default changePassword