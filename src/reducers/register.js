const initialState = {
    email: '',
    error:null,
    token:'',
    loading:false
}

const registerAccount = (state=initialState, action)=>{
    switch(action.type){
        case 'REQUEST_NEW_ACCOUNT':
            return{ loading:true }
        case 'SUCCESS_NEW_ACCOUNT':
            return {email: action.user.email , error:null, loading:false}; 
        case 'FAILURE_NEW_ACCOUNT': 
            return {error:action.err, loading:false}               
        default:
            return state;
    }
}

export default registerAccount