const initialState = {
    data:{

    },
    token: null
}

const rootReducer = (state = initialState, action) => {
    switch(action.type){
        case 'GET_TOKEN':
            return { token: action.data.token, data: action.data };
        case 'SET_TOKEN':
            return { token: action.token };
        case 'UNSET_TOKEN':
            return { token: null };
        default:
            return state;            
    }
}

export default rootReducer