import { createStore, applyMiddleware, combineReducers} from 'redux'
import thunk from 'redux-thunk'
import register from '../reducers/register'
import session from '../reducers/session'
const createStoreWitchMiddleware = applyMiddleware(thunk)
(createStore);

const reducers = combineReducers({
    registerState : register,
    sessionState : session
})

const store = createStoreWitchMiddleware(reducers)

export default store