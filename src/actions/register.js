import Axios from 'axios'
import {BASE_URL} from '../services/config'
const REQUEST_NEW_ACCOUNT = (user) => ({type:'REQUEST_NEW_ACCOUNT',user})
const SUCCESS_NEW_ACCOUNT = (user) => ({type:'SUCCESS_NEW_ACCOUNT',user})
const FAILURE_NEW_ACCOUNT = (err) => ({type:'FAILURE_NEW_ACCOUNT',err})
export const registerAccount = (user) => {
    console.log(user)
    return async dispatch => {
        dispatch(REQUEST_NEW_ACCOUNT(user));
        try {
            const post = await Axios({
                method: 'post',
                url: BASE_URL+'/users/create',
                data: user,
                })
                if (await post){
                    const data = post.data
                    console.log(post.data)
                    dispatch(SUCCESS_NEW_ACCOUNT(data))
                }
        } catch(err){
            console.log(err.response.data)
            dispatch(FAILURE_NEW_ACCOUNT(err))
        }
    }
}
