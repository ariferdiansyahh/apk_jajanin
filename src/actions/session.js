import { AsyncStorage } from 'react-native'
import axios from 'axios'
import { BASE_URL } from '../services/config'
export const GET_TOKEN = (token) => ({type: 'GET_TOKEN', token});
export const SET_TOKEN = (data) => ({type: 'SET_TOKEN', data});
export const UNSET_TOKEN = (token) => ({type: 'UNSET_TOKEN' , token})

export const loginAccount = (user) => {
    console.log(user)
    return async dispatch => {
        try{
            const post = await Axios({
                method: 'post',
                url: BASE_URL+'/users/login',
                data: user,
                })
                if (await post){
                    const data = post.data
                    console.log(post.data)
                    AsyncStorage.setItem('token', data.token)
                    dispatch(SET_TOKEN(data))
                }
        } catch(err){
            console.log(err.response.data)
        }
    }
}

export const getSession = () => {
    return async dispatch => {
        try{
            const token = AsyncStorage.getItem('token')
            if(token){
                dispatch(GET_TOKEN(token))
            }
        } catch(err){
            console.log(err)
        }
    }
}

export const logoutUser = () =>{
    try{
        const removeToken = AsyncStorage.removeItem('token')
        if(!removeToken){
            dispatch(UNSET_TOKEN())
        }
    } catch(err){
        console.log(err)
    }
}
