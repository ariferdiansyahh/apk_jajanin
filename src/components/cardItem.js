import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import ImageLoad from 'react-native-image-placeholder';

const {height, width} = Dimensions.get('window')

const FULL_WIDTH = width

const loadingStyle = {
  size: 'large',
  color: 'grey',
};

class cardItem extends Component {
  render() {
    const { title, imageUri } = this.props;
    return (
        <View style={styles.vwImageLocation}>
            <ImageLoad
            style={styles.imageLocation}
            loadingStyle={loadingStyle}
            source={this.props.imageUri}
            />
            <Text style={styles.titleTextLocation2}>{this.props.name}</Text>
            <Text style={styles.titleTextLocation}>Rp. {this.props.price}</Text>
        </View>           
    );
  }
}

export default cardItem;

const styles = StyleSheet.create({
  vwImageLocation: {
    width: FULL_WIDTH - 60,
    height: 200,
    marginTop: 20,    
  },
  imageLocation: {
    flex: 1,
    height: null,
    width: null,
    marginRight: 10,
    resizeMode: 'cover',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#dddddd",
  },
  titleTextLocation: {
    position: 'absolute',
    right: 5,
    bottom: 0,
    paddingRight: 20,
    paddingLeft: 10,
    paddingBottom: 10,
    fontWeight: 'bold',
    color: "white",
    fontSize: 24,
    textShadowColor: "black",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
  titleTextLocation2: {
    position: 'absolute',
    left: 5,
    bottom: 0,
    paddingRight: 20,
    paddingLeft: 10,
    paddingBottom: 10,
    fontWeight: 'bold',
    color: "white",
    fontSize: 24,
    textShadowColor: "black",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
});
