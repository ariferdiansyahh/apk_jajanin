import React, { Component } from 'react';
import { Image, StyleSheet } from 'react-native';

export default class LogoTitle extends Component{
    render(){
        return(
            <Image source={require('../img/logo-blue.png')} style={styles.logo} resizeMode='contain'/>
        )
    }
}

const styles = StyleSheet.create({
    logo:{
        height: 35,     
    }
});