import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, Button , TouchableOpacity} from 'react-native';
import { connect } from 'react-redux'
import { Icon } from 'native-base';
import {COLORS} from '../services/config'
class detailsCard extends Component{
    render (){
        return(
            <TouchableOpacity>
            <View style={styles.card}>
                <Image source={this.props.imageUri} style={styles.cardImage} />
                <View style={styles.cardBody}>
                    <View style={styles.menu}>
                        <Text style={styles.menuName}>{this.props.name}</Text>
                        <Text>{this.props.type}</Text>
                    </View>
                    <View>
                    <Text style={styles.menuPrice}>Rp. {this.props.price}</Text>
                    </View>
                </View>
            </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    card: {flexDirection: 'row', height: 60, flex: 1, borderWidth: 0.5, borderColor: 'grey', borderRadius: 5, marginTop: 10},
    cardImage: {width: 60, height: 60, borderTopLeftRadius: 5, borderBottomLeftRadius: 5},
    cardBody: {flexDirection: 'row', justifyContent: 'space-between', flex: 1, padding: 10},
    menu: {alignSelf: 'center'},
    menuName: {color: 'black', fontWeight: 'bold'},
    menuPrice: {alignSelf: 'center', color: COLORS.PRIMARY_COLOR}
});

export default detailsCard;